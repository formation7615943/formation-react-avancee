# React + TypeScript + Vite

This code is used as a demonstration for the "Formation React Avancee" on 23th of October 2023

This code represents a simple task manager where you can add, update and delete tasks.

## Actions

Currently the first data are mocked, we received them after 850ms.  
We can add more tasks, remove and/or update them

### Update

The update function enable to edit the title, and the deletion remove the selected

### Deletion

The deletion works by selecting one and delete them.
