import { useState, useEffect, useContext } from "react";
import { TaskType } from "../components/task/TaskNote";
import { TaskForm } from "../components/taskform/TaskForm";
import { TaskList } from "../components/tasklist/TaskList";
import * as taskapi from "../api/task2.service";
import { userContext } from "../context/UserContext";

export const TaskPage = () => {
  const [tasks, setTasks] = useState<TaskType[]>([]);
  const [display, setDisplay] = useState(true);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [searchValue, setSearchValue] = useState("");
  const user = useContext(userContext);

  useEffect(() => {
    taskapi
      .getTasks()
      .then((res) => {
        setLoading(false);
        setTasks(res);
      })
      .catch((err) => {
        setLoading(false);
        setError(true);
        console.warn(err);
      });
  }, []);

  // console.log("TaskPage rendering");
  // useEffect(() => {
  //   let isCancel = false;
  //   console.log("TaskPage ue -", searchValue);
  //   if (searchValue)
  //     taskapi.getTaskByTitle(searchValue).then((tasks) => {
  //       if (!isCancel) setTasks(tasks);
  //     });
  //   else setTasks([]);
  //   // else taskapi.getTasks().then((tasks) => setTasks(tasks));

  //   return () => {
  //     isCancel = true;
  //   };
  // }, [searchValue]);

  const addTask = (
    title: string,
    duration: number = Math.round(Math.random() * 100),
    type?: string,
    startDate?: string,
    description?: string
  ) => {
    const diff = Math.round(Math.random() * 10);
    const details = {
      level: "Level " + diff,
      difficulty: diff,
    };
    taskapi
      .addTask({ title, duration, details, startDate, type, description })
      .then((tasks) => {
        setTasks(tasks);
      });
    // setTasks((prevTasks) => {
    //   return [
    //     ...prevTasks,
    //     {
    //       id:
    //         prevTasks.length > 0
    //           ? Math.max(...prevTasks.map((task) => task._id)) + 1
    //           : 0,
    //       title: title,
    //       duration: duration,
    //       details: {
    //         level: "Level " + diff,
    //         difficulty: diff,
    //       },
    //     },
    //   ];
    // });
  };

  const updateTask = (id: number, title: string, duration: number) => {
    taskapi
      .updateTask(id, { title: title, duration: duration })
      .then((tasks) => setTasks(tasks));
    // setTasks((prevTasks) => {
    //   return prevTasks.map((task) => {
    //     if (task._id === id) {
    //       task.title = title;
    //       task.duration = duration;
    //     }
    //     return task;
    //   });
    // });
  };

  const removeTask = (id: number) => {
    // setTasks((prevTasks) => prevTasks.filter((task) => task._id !== id));
    taskapi.deleteTask(id).then((tasks) => setTasks(tasks));
  };

  const changeTasksVisibility = () => {
    setDisplay((prevDisplay) => !prevDisplay);
  };

  if (error) return <div>An error occurs, contact support</div>;

  return loading ? (
    <div>Loading ...</div>
  ) : (
    <>
      <div>Welcome {user?.name}</div>
      <TaskForm addTask={addTask} />
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <div>
          <label htmlFor="filter">Filter tasks</label>
          <input
            name="filter"
            type="text"
            value={searchValue}
            onChange={(e) => setSearchValue(e.target.value)}
          />
        </div>
        <div>
          <button onClick={() => changeTasksVisibility()}>
            {display ? "Hide tasks" : "Display tasks"}
          </button>
        </div>
      </div>
      {display && (
        <TaskList tasks={tasks} onUpdate={updateTask} onRemove={removeTask} />
      )}
    </>
  );
};
