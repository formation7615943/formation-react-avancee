import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { fetchTaskById } from "../api/task.api";
import { TaskType } from "../components/task/TaskNote";

export const TaskDetail = () => {
  // Get the userId param from the URL.
  const { taskId } = useParams();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [task, setTask] = useState<TaskType | undefined>();

  useEffect(() => {
    setLoading(true);
    fetchTaskById(taskId)
      .then((task) => {
        setTask(task);
        setLoading(false);
      })
      .catch((err) => {
        console.warn(err);
        setError(true);
      });
  }, [taskId]);

  if (error)
    return (
      <div>
        <p>Ooops sorry, an error occured.</p>
        <Link to={"/tasks"}>Return to tasks list</Link>
      </div>
    );

  return (
    <div>
      <div>{`Task number: ${taskId}`}</div>
      {loading && <div>Loading...</div>}
      {task && (
        <div>
          <div>Title: {task.title}</div>
          <div>Duration: {task.duration}</div>
          <div>
            Details:
            <div>{task.details?.difficulty}</div>
            <div>{task.details?.level}</div>
          </div>
          <div>{task.startDate}</div>
          <div>{task.type}</div>
          <div>{task.description}</div>
        </div>
      )}
    </div>
  );
};
