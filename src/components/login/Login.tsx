import { useState } from "react";
import * as api from "../../api/auth.api";
import { useNavigate } from "react-router-dom";

export const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const handleLogin = () => {
    api
      .login({ email, password })
      .then((token) => {
        console.log("Receive token", token);
        localStorage.setItem("token", token);
        navigate("/");
      })
      .catch((err) => {
        console.warn("Error during log in", err);
        setError(true);
      });
  };

  if (error) return <div>Unexpected error during logging</div>;

  return (
    <div style={{ display: "flex", flexDirection: "column", gap: "20px" }}>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <label htmlFor="login">Email</label>
        <input
          type="email"
          name="login"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        <label htmlFor="password">Password</label>
        <input
          type="password"
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <button type="button" onClick={handleLogin}>
        Log in
      </button>
    </div>
  );
};
