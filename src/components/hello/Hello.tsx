import { useParams } from "react-router-dom";
import "./hello.scss";

import { useEffect } from "react";
import { me } from "../../api/auth.api";

export const Hello = () => {
  const { role } = useParams();
  useEffect(() => {
    me().then((coucou) => console.log(coucou));
  }, []);

  return <div className="hello">Hello {role}</div>;
};
