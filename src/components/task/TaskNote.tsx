import clsx from "clsx";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export type TaskType = {
  _id: number;
  title: string;
  duration: number;
  details: Details;
  startDate?: string;
  type?: string;
  description?: string;
};

export type Details = {
  difficulty: number;
  level: string;
};

export type TaskActions = {
  onUpdate?: (id: number, title: string, duration: number) => void;
  onDelete?: (id: number) => void;
};

export const TaskNote = (props: TaskType & TaskActions) => {
  const [newTitle, setNewTitle] = useState(props.title);
  const [newDuration, setNewDuration] = useState(props.duration);
  const [isEditing, setIsEditing] = useState(false);
  const navigate = useNavigate();

  const editTask = () => {
    props.onUpdate?.(props._id, newTitle, newDuration);
    setIsEditing(!isEditing);
  };

  return (
    <div className={clsx("task", props.duration > 70 && "customTask")}>
      {isEditing ? (
        <>
          <input
            value={newTitle}
            onChange={(e) => setNewTitle(e.target.value)}
          />
          <input
            type="number"
            value={newDuration}
            onChange={(e) => setNewDuration(+e.target.value)}
          />
        </>
      ) : (
        <div style={{ display: "block" }}>
          <div
            className="title"
            onClick={() => {
              navigate(`${props._id}`);
            }}
          >{`${props.type ? props.type + " - " : ""}${props.title}`}</div>
          <div>
            <div className="diff">{`Duration: "${props.duration}"`}</div>
          </div>
          <div>{props.description && <div>{props.description}</div>}</div>
          <div className="level">{`Diff: ${
            props.details.difficulty ?? "?"
          } - Level ${props.details.level ?? "unkown"}`}</div>
        </div>
      )}
      <div className="actions">
        {!isEditing && (
          <button onClick={() => props.onDelete?.(props._id)}>Delete</button>
        )}
        <button onClick={editTask}>{isEditing ? "Validate" : "Update"}</button>
      </div>
    </div>
  );
};
