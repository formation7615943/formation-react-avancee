import { useState } from "react";

type TTaskForm = {
  addTask: (
    title: string,
    duration?: number,
    startDate?: string,
    type?: string,
    description?: string
  ) => void;
};

export const TaskForm = (props: TTaskForm) => {
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");
  const [type, setType] = useState("");
  const [startDate, setStartDate] = useState("");
  const [duration, setDuration] = useState<number>(0);

  return (
    <form className="form">
      <div className="inputs">
        <div className="labelInput">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            name="title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="labelInput">
          <label htmlFor="type">Type</label>
          <input
            type="text"
            name="type"
            value={type}
            onChange={(e) => setType(e.target.value)}
          />
        </div>
        <div className="labelInput">
          <label htmlFor="startDate">Start Date</label>
          <input
            type="date"
            name="startDate"
            value={startDate}
            onChange={(e) => setStartDate(e.target.value)}
          />
        </div>
        <div className="labelInput">
          <label htmlFor="duration">Duration</label>
          <input
            name="duration"
            value={duration}
            onChange={(e) => setDuration(+e.target.value)}
          />
        </div>
        <div className="labelInput">
          <label htmlFor="desc">Description</label>
          <textarea
            name="desc"
            rows={3}
            value={desc}
            onChange={(e) => setDesc(e.target.value)}
          />
        </div>
      </div>

      <div className="right">
        <button
          onClick={(e) => {
            e.preventDefault();
            props.addTask(
              title,
              duration == 0 ? undefined : duration,
              startDate,
              type,
              desc
            );
            setTitle("");
            setDuration(0);
          }}
        >
          Add a task
        </button>
      </div>
    </form>
  );
};
