import { Link } from "react-router-dom";

export const WhoAreU = () => {
  return (
    <div>
      <p>
        Hi there, in order to redirect you to the best path, tell me who you are
      </p>
      <ul>
        <li>
          <Link to="/teacher">I am a teacher</Link>
        </li>
        <li>
          <Link to="/student">I am a student</Link>
        </li>
      </ul>
    </div>
  );
};
