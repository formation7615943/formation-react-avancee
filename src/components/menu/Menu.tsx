import { NavLink, useParams } from "react-router-dom";
import { Hello } from "../hello/Hello";
export const Menu = () => {
  const { role } = useParams();
  return (
    <div>
      {role === "teacher" && (
        <ul>
          <li>
            <NavLink
              to="tasks"
              style={(params) =>
                params.isActive ? { color: "red" } : undefined
              }
            >
              Tasks
            </NavLink>
          </li>
          <li>
            <NavLink
              to=".."
              style={(params) =>
                params.isActive ? { color: "red" } : undefined
              }
            >
              Retour
            </NavLink>
          </li>
        </ul>
      )}
      <Hello />
    </div>
  );
};
