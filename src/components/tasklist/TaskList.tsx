import { useEffect } from "react";
import { TaskNote, TaskType } from "../task/TaskNote";

export const TaskList = ({
  tasks,
  onUpdate,
  onRemove,
}: {
  tasks: TaskType[];
  onUpdate: (
    id: number,
    title: string,
    duration: number,
    startDate?: string,
    type?: string,
    desc?: string
  ) => void;
  onRemove: (id: number) => void;
}) => {
  useEffect(() => {
    console.log("new taks in ue", tasks);
  }, [tasks]);
  return (
    <div style={{ display: "flex", flexDirection: "row", flexWrap: "wrap" }}>
      {tasks?.map((elt) => (
        <TaskNote
          key={elt._id}
          _id={elt._id}
          title={elt.title}
          duration={elt.duration}
          details={{
            level: elt.details?.level,
            difficulty: elt.details?.difficulty,
          }}
          onDelete={onRemove}
          onUpdate={onUpdate}
        />
      ))}
    </div>
  );
};
