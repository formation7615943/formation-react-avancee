import { PrivateRoute } from "../PrivateRoute";
import { Menu } from "../menu/Menu";

export const Layout = () => {
  return (
    <div>
      <header>This is a header</header>
      <nav>
        <Menu />
      </nav>

      <PrivateRoute />

      <footer>Copyright Formation React - cuvée 2023</footer>
    </div>
  );
};
