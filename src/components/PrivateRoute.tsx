import { Outlet, useParams } from "react-router-dom";

export const PrivateRoute = () => {
  const { role } = useParams();
  return role === "teacher" ? <Outlet /> : <div>Not Allowed</div>;
};
