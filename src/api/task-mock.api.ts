import { Details, TaskType } from "../components/task/TaskNote";

const hardCodedTasks: TaskType[] = [
  {
    _id: 1,
    title: "Learn HTML",
    duration: 10,
    details: { difficulty: 1, level: "easy" },
  },
  {
    _id: 2,
    title: "Learn React",
    duration: 45,
    details: { difficulty: 8, level: "medium" },
  },
  {
    _id: 3,
    title: "Learn Java",
    duration: 71,
    details: { difficulty: 7, level: "medium" },
  },
];

const delay = (timeMs: number): Promise<void> => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeMs);
  });
};

export const getTasks = async (): Promise<TaskType[]> => {
  await delay(850);
  // throw new Error("Error while fetching");
  return hardCodedTasks;
};

export const getTask = async (id: number): Promise<TaskType[]> => {
  await delay(850);
  return hardCodedTasks.filter((task) => task._id === id);
};

export const getTaskByTitle = async (
  searchValue: string
): Promise<TaskType[]> => {
  await delay(850);
  return hardCodedTasks.filter((task) =>
    task.title.toLowerCase().includes(searchValue.toLowerCase())
  );
};

export const deleteTask = async (id: number): Promise<TaskType[]> => {
  await delay(850);
  return hardCodedTasks.filter((task) => task._id !== id);
};

export const addTask = async (
  title: string,
  duration: number,
  details: Details,
  startDate?: string,
  type?: string,
  description?: string
): Promise<TaskType[]> => {
  await delay(500);
  hardCodedTasks.push({
    _id: Math.round(Math.max(...hardCodedTasks.map((task) => task._id))) + 1,
    title: title,
    duration: duration,
    details: details,
    startDate: startDate,
    type: type,
    description: description,
  });
  return [...hardCodedTasks];
};

export const updateTask = async (
  id: number,
  taskToUpdate: Partial<TaskType>
) => {
  await delay(1000);
  const newTasks = hardCodedTasks.map((task) => {
    if (task._id === id) {
      const updatedTask = { ...task, ...taskToUpdate };
      return updatedTask;
    }
    return task;
  });

  return newTasks;
};
