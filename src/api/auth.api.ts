import { default as Axios } from "axios";
import { header } from "../utils/auth.utils";

export const login = async (credentials: {
  email: string;
  password: string;
}): Promise<string> => {
  const result = await Axios.post(
    "http://localhost:5000/api/v1/auth/login",
    credentials
  );
  return result.data.token;
};
export const me = async (): Promise<any> => {
  // await delay(500)
  const result = await Axios.get(
    "http://localhost:5000/api/v1/auth/me",
    header()
  );
  return result.data.model;
};
