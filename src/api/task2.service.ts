import Axios from "axios";
import { TaskType } from "../components/task/TaskNote";

export const addTask = async (task: Partial<TaskType>): Promise<TaskType[]> => {
  const result = await Axios.post("http://localhost:5000/api/v1/tasks", task);
  return result.data.model;
};

export const updateTask = async (
  id: number,
  task: Partial<TaskType>
): Promise<TaskType[]> => {
  const result = await Axios.put(
    "http://localhost:5000/api/v1/tasks/" + id,
    task
  );
  return result.data.model;
};

export const deleteTask = async (id: number): Promise<TaskType[]> => {
  const result = await Axios.delete("http://localhost:5000/api/v1/tasks/" + id);
  return result.data.model;
};

export const getTaskById = async (taskId: number): Promise<TaskType[]> => {
  const result = await Axios.get(
    "http://localhost:5000/api/v1/tasks/" + taskId
  );
  return result.data.model;
};

export const getTasks = async (): Promise<TaskType[]> => {
  // await delay(500)
  const result = await Axios.get("http://localhost:5000/api/v1/tasks");
  return result.data.model;
};
