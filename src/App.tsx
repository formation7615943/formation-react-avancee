import "./styles.scss";
import { TaskPage } from "./pages/TaskPage";
import {
  Navigate,
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";
import { TaskDetail } from "./pages/TaskDetail";

import { Layout } from "./components/layout/Layout";
import { Login } from "./components/login/Login";
import { useEffect, useState } from "react";
import { me } from "./api/auth.api";
import { userContext } from "./context/UserContext";

const App = () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const [user, setUser] = useState<any>();
  const token = localStorage.getItem("token");

  useEffect(() => {
    me().then((user) => {
      setUser(user);
    });
  }, []);

  let status;
  if (!token) {
    status = "token";
  } else if (user?.role == "admin") {
    status = "admin";
  } else if (user?.role == "user") {
    status = "user";
  } else {
    status = "NeitherAdminOrUser";
  }
  console.log(status);

  const router = createBrowserRouter([
    {
      path: "/",
      element: (
        <Navigate
          to={
            !token
              ? "/login"
              : token && user?.role === "admin"
              ? "/teacher"
              : token && user?.role === "user"
              ? "/student"
              : "/login"
          }
        />
      ),
    },
    {
      path: "/login",
      element: <Login />,
    },
    {
      path: "/:role",
      element: (
        <userContext.Provider value={user}>
          <Layout />
        </userContext.Provider>
      ),
      children: [
        {
          path: "tasks",
          children: [
            {
              element: <TaskPage />,
              index: true,
            },
            {
              path: ":taskId",
              element: <TaskDetail />,
            },
          ],
        },
      ],
    },
  ]);

  return <RouterProvider router={router} />;
};

export default App;
